package dev.vrba.certy.rytir.api.request

import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min

data class DeckRequest(
    @Min(1)
    @Max(200)
    val cards: Map<String, Int>,
)
