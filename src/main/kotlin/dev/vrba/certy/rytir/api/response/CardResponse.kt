package dev.vrba.certy.rytir.api.response

import com.fasterxml.jackson.annotation.JsonProperty
import dev.vrba.certy.rytir.api.domain.CardScrapeResult
import dev.vrba.certy.rytir.api.response.dto.ScrapedCardResource
import dev.vrba.certy.rytir.api.response.dto.toResource

data class CardResponse(
    @JsonProperty("input_query")
    val input: String,
    @JsonProperty("cards")
    val cards: List<ScrapedCardResource>,
    @JsonProperty("median_price")
    val medianPrice: Int,
    @JsonProperty("requested_count")
    val requestedCount: Int,
    @JsonProperty("total_pieces_in_stock")
    val totalPiecesInStock: Int,
    @JsonProperty("all_cards_in_stock")
    val allCardsInStock: Boolean,
)

fun CardScrapeResult.toResponse() =
    CardResponse(
        input = input,
        cards = cards.map { it.toResource() },
        medianPrice = medianPrice,
        requestedCount = requestedCount,
        totalPiecesInStock = totalPiecesInStock,
        allCardsInStock = allCardsInStock,
    )
