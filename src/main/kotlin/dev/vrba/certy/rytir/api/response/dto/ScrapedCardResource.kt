package dev.vrba.certy.rytir.api.response.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonUnwrapped
import dev.vrba.certy.rytir.api.domain.ScrapedCard

data class ScrapedCardResource(
    @JsonProperty("card_name")
    val name: String,
    @JsonProperty("card_type")
    val type: String,
    @JsonProperty("card_image_url")
    val image: String,
    @JsonUnwrapped
    val set: ScrapedCardSetResource,
    @JsonProperty("card_rarity")
    val rarity: String,
    @JsonProperty("card_price")
    val price: Int,
    @JsonProperty("card_pieces_in_stock")
    val piecesInStock: Int,
)

fun ScrapedCard.toResource() =
    ScrapedCardResource(
        name = name,
        type = type,
        image = image,
        set = set.toResource(),
        rarity = rarity.value,
        price = price,
        piecesInStock = piecesInStock,
    )
