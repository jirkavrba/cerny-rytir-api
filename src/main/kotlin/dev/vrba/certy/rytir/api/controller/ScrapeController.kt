package dev.vrba.certy.rytir.api.controller

import dev.vrba.certy.rytir.api.request.CardRequest
import dev.vrba.certy.rytir.api.request.DeckRequest
import dev.vrba.certy.rytir.api.response.CardResponse
import dev.vrba.certy.rytir.api.response.DeckResponse
import dev.vrba.certy.rytir.api.response.toResponse
import dev.vrba.certy.rytir.api.service.ScrapingService
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/api/scrape")
class ScrapeController(private val service: ScrapingService) {
    @PostMapping("/card")
    suspend fun card(
        @Valid @RequestBody request: CardRequest,
    ): ResponseEntity<CardResponse> {
        return service.scrapeCard(request.input)
            ?.let { ResponseEntity.ok(it.toResponse()) }
            ?: ResponseEntity.unprocessableEntity().build()
    }

    @PostMapping("/deck")
    suspend fun deck(
        @Valid @RequestBody request: DeckRequest,
    ): ResponseEntity<DeckResponse> {
        val result = service.scrapeDeck(request.cards)
        val response = result.toResponse()

        return ResponseEntity.ok(response)
    }
}
