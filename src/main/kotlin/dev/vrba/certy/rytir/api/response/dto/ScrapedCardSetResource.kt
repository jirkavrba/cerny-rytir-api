package dev.vrba.certy.rytir.api.response.dto

import com.fasterxml.jackson.annotation.JsonProperty
import dev.vrba.certy.rytir.api.domain.CardSet

data class ScrapedCardSetResource(
    @JsonProperty("card_set_name")
    val name: String,
    @JsonProperty("card_set_icon")
    val icon: String,
)

fun CardSet.toResource() =
    ScrapedCardSetResource(
        name = name,
        icon = icon,
    )
