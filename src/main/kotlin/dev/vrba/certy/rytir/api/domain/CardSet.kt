package dev.vrba.certy.rytir.api.domain

data class CardSet(
    val name: String,
    val icon: String,
)
