package dev.vrba.certy.rytir.api.domain

data class CardScrapeResult(
    val input: String,
    val cards: List<ScrapedCard>,
    val medianPrice: Int,
    val requestedCount: Int,
    val totalPiecesInStock: Int,
    val allCardsInStock: Boolean,
)
