package dev.vrba.certy.rytir.api.response

import com.fasterxml.jackson.annotation.JsonProperty
import dev.vrba.certy.rytir.api.domain.DeckScrapeResult

data class DeckResponse(
    @JsonProperty("cards")
    val cards: List<CardResponse>,
    @JsonProperty("median_deck_price")
    val medianDeckPrice: Int,
    @JsonProperty("all_cards_in_stock")
    val allCardsInStock: Boolean,
)

fun DeckScrapeResult.toResponse() =
    DeckResponse(
        cards = cards.sortedBy { it.totalPiecesInStock }.map { it.toResponse() },
        medianDeckPrice = medianDeckPrice,
        allCardsInStock = allCardsInStock,
    )
