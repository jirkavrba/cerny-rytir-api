package dev.vrba.certy.rytir.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CernyRytirApiApplication

fun main(args: Array<String>) {
    runApplication<CernyRytirApiApplication>(*args)
}
