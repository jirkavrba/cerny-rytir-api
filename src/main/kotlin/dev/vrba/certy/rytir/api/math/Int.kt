package dev.vrba.certy.rytir.api.math

fun Iterable<Int>.median(): Int =
    sorted().let {
        if (it.isEmpty()) {
            return 0
        }

        if (it.size % 2 == 0) {
            (it[it.size / 2] + it[(it.size - 1) / 2]) / 2
        } else {
            it[it.size / 2]
        }
    }
