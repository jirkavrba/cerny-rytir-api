package dev.vrba.certy.rytir.api.domain

enum class CardRarity(val value: String) {
    Common("common"),
    Uncommon("uncommon"),
    Rare("rare"),
    Mythic("mythic"),
}
