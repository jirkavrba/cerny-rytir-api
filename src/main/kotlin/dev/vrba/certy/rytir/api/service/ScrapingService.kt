package dev.vrba.certy.rytir.api.service

import dev.vrba.certy.rytir.api.domain.CardRarity
import dev.vrba.certy.rytir.api.domain.CardScrapeResult
import dev.vrba.certy.rytir.api.domain.CardSet
import dev.vrba.certy.rytir.api.domain.DeckScrapeResult
import dev.vrba.certy.rytir.api.domain.ScrapedCard
import dev.vrba.certy.rytir.api.math.median
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import org.jsoup.Jsoup
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.util.MultiValueMapAdapter
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody

@Service
class ScrapingService {
    private val client = WebClient.builder().baseUrl("https://cernyrytir.cz/").build()

    private val logger = LoggerFactory.getLogger(this::class.qualifiedName)

    suspend fun scrapeCard(
        input: String,
        count: Int = 1,
        filterCardsNotInStock: Boolean = false,
    ): CardScrapeResult? {
        try {
            logger.info("Scraping card with input query: [{}]", input)

            val raw = fetchSearchResultsHtml(input)
            val cards = parseSearchResultsHtml(raw)
            val filtered = if (filterCardsNotInStock) cards.filter { it.piecesInStock > 0 } else cards
            val sorted = filtered.sortedByDescending { it.piecesInStock }

            logger.info(
                "Finished scraping card with input query [{}], found [{}] cards {}",
                input,
                sorted.size,
                if (filterCardsNotInStock) "with pieces in stock" else "total",
            )

            val totalPiecesInStock = sorted.sumOf { it.piecesInStock }

            return CardScrapeResult(
                input = input,
                cards = sorted,
                medianPrice = sorted.map { it.price }.median(),
                requestedCount = count,
                totalPiecesInStock = totalPiecesInStock,
                allCardsInStock = count <= totalPiecesInStock,
            )
        } catch (throwable: Throwable) {
            logger.error(
                "Error scraping card with input query [{}]",
                input,
                throwable,
            )

            return null
        }
    }

    @OptIn(ExperimentalStdlibApi::class)
    suspend fun scrapeDeck(cards: Map<String, Int>): DeckScrapeResult {
        logger.info("Scraping deck with [{}] cards that hashes to [{}]", cards.size, cards.hashCode().toHexString())

        val result =
            coroutineScope {
                cards
                    .entries
                    .distinctBy { it.key }
                    .map { async { scrapeCard(it.key, it.value, true) } }
                    .awaitAll()
                    .filterNotNull()
            }

        logger.info("Finished scraping deck with hash [{}]", cards.hashCode().toHexString())

        val medianDeckPrice =
            cards.entries.sumOf { (query, quantity) ->
                // Exclude basic lands
                val match = result.first { it.input == query }

                if (match.cards.any { it.type.lowercase().contains("basic land") }) {
                    0
                } else {
                    match.medianPrice * quantity
                }
            }

        val allCardsInStock =
            cards.all { (query, quantity) ->
                result.first { it.input == query }.cards.sumOf { it.piecesInStock } >= quantity
            }

        return DeckScrapeResult(
            cards = result,
            medianDeckPrice = medianDeckPrice,
            allCardsInStock = allCardsInStock,
        )
    }

    private suspend fun fetchSearchResultsHtml(input: String): String {
        val request =
            MultiValueMapAdapter(
                mapOf(
                    "hledej_pouze_magic" to listOf("1"),
                    "edice_magic" to listOf("libovolna"),
                    "rarita" to listOf("A"),
                    "foil" to listOf("A"),
                    "triditpodle" to listOf("ceny"),
                    "submit" to listOf("Vyhledej"),
                    "jmenokarty" to listOf(input),
                ),
            )

        return client.post()
            .uri("index.php3?akce=3")
            .body(BodyInserters.fromFormData(request))
            .retrieve()
            .awaitBody<String>()
    }

    private suspend fun parseSearchResultsHtml(raw: String): List<ScrapedCard> {
        try {
            val body = Jsoup.parse(raw).body()
            val (_, table) = body.select("table.kusovkytext")
            val cards = table.select("tbody > tr").chunked(3)

            return cards.map { rows ->
                val (top, middle, bottom) = rows

                val image = top.select("td:nth-child(1) > a").attr("href")
                val name = top.select("td:nth-child(2) > div > font").text()

                val setName = middle.select("td:nth-child(1)").textNodes().first().text()
                val setIcon = middle.select("td:nth-child(1) > img").attr("src")
                val type = middle.select("td:nth-child(2)").text().replace("�", "-")

                val rarity =
                    when (bottom.select("td:nth-child(1)").text()) {
                        "Mythic" -> CardRarity.Mythic
                        "Rare" -> CardRarity.Rare
                        "Uncommon" -> CardRarity.Uncommon
                        else -> CardRarity.Common
                    }

                val piecesInStock = bottom.select("td:nth-child(2) > font").text().trim().takeWhile { it.isDigit() }.toInt()
                val price = bottom.select("td:nth-child(3) > font").text().trim().takeWhile { it.isDigit() }.toInt()

                ScrapedCard(
                    name = name,
                    type = type,
                    image = image.withDomainPrefix(),
                    set =
                        CardSet(
                            name = setName,
                            icon = setIcon.withDomainPrefix(),
                        ),
                    rarity = rarity,
                    price = price,
                    piecesInStock = piecesInStock,
                )
            }
        } catch (throwable: Throwable) {
            logger.error("Error parsing HTML", throwable)
            return emptyList()
        }
    }

    private fun String.withDomainPrefix() = "https://cernyrytir.cz/$this".replace("(?<!:)/+".toRegex(), "/")
}
