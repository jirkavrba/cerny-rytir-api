package dev.vrba.certy.rytir.api.domain

data class ScrapedCard(
    val name: String,
    val type: String,
    val image: String,
    val set: CardSet,
    val rarity: CardRarity,
    val price: Int,
    val piecesInStock: Int,
)
