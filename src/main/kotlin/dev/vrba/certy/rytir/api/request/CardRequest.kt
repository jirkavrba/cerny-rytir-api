package dev.vrba.certy.rytir.api.request

import jakarta.validation.constraints.NotBlank

data class CardRequest(
    @NotBlank
    val input: String,
)
