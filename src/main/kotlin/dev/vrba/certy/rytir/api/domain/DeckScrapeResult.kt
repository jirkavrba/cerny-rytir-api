package dev.vrba.certy.rytir.api.domain

data class DeckScrapeResult(
    val cards: List<CardScrapeResult>,
    val medianDeckPrice: Int,
    val allCardsInStock: Boolean,
)
